#include "Bishop.h"

Bishop::Bishop(const bool isBlack, const  Point p) : Piece(isBlack, p)
{
	_type = 'b';
}

int Bishop::validateMove(const Point newPoint)
{
	int ratioLeft = _loc._y - _loc._x;
	int ratioRight = _loc._y + _loc._x;
	if (ratioLeft == newPoint._y - newPoint._x ||
		ratioRight == newPoint._y + newPoint._x )
	{
		return VALID_MOVE;
	}
	return NOT_VALID_PIECE_ILLIGAL;
}

std::vector<Point*> Bishop::getBetween(Point newPoint)
{
	std::vector<Point*> myVec;
	Point direction = newPoint - _loc;
	int y = _loc._y;
	int x = _loc._x;
	while ( x != newPoint._x)
	{
		direction._x < 0 ? x-- : x++;
		direction._y < 0 ? y-- : y++;
		myVec.push_back(new Point(y, x));
	}
	if (myVec.size() > 0)
	{
		delete myVec.back();
		myVec.pop_back(); //because the last point would be newPoint
	}
	return myVec;
}
