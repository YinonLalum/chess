#pragma once

#include "Piece.h"
class Bishop : public virtual Piece
{
public:
	Bishop(const bool isBlack, const  Point p);
	int validateMove(const Point newPoint);
	std::vector<Point*> getBetween(Point newPoint);


};


