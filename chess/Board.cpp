#include "Board.h"

using namespace std;

Board::Board(std::string str)
{
	Piece* piece = nullptr;
	for (int i = 0; i <= BOARD_SIDE; i++)
	{
		for (int j = 0; j <= BOARD_SIDE; j++)
		{
			switch (str[i*(BOARD_SIDE+1)+j])
			{
			case 'r':
				piece = new Rook(true,Point(i,j));
				break;
			case 'R':
				piece = new Rook(false, Point(i, j));
				break;
			case 'n':
				piece = new Knight(true, Point(i, j));
				break;
			case 'N':
				piece = new Knight(false, Point(i, j));
				break;
			case 'b':
				piece = new Bishop(true, Point(i, j));
				break;
			case 'B':
				piece = new Bishop(false, Point(i, j));
				break;
			case 'k':
				piece = new King(true, Point(i, j));
				break;
			case 'K':
				piece = new King(false, Point(i, j));
				break;
			case 'q':
				piece = new Queen(true, Point(i, j));
				break;
			case 'Q':
				piece = new Queen(false, Point(i, j));
				break;
			case '#':
				piece = nullptr;
				break;
			case 'p':
				piece = new Pawn(true, Point(i, j));
				break;
			case 'P':
				piece = new Pawn(false, Point(i, j));
				break;
			default:
				break;
			}
			_pieces[i][j] = piece;
		}
	}
}

Board::Board(Board & b)
{
	for (int i = 0; i <= BOARD_SIDE; i++)
	{
		for (int j = 0; j <= BOARD_SIDE; j++)
		{
			_pieces[i][j] = b._pieces[i][j];
		}
	}
}

Board::~Board()
{
	for (int i = 0; i <= BOARD_SIDE; i++)
	{
		for (int j = 0; j <= BOARD_SIDE; j++)
		{
			delete _pieces[i][j];
		}
	}
}

string Board::getBoardStr() const
{
	string str;
	Piece* p = nullptr;
	char c = 0;
	for (int i = 0; i <= BOARD_SIDE; i++)
	{
		for (int j = 0; j <= BOARD_SIDE; j++)
		{
			p = _pieces[i][j];
			if (p)
			{
				c = p->getType();
				if (!p->isBlack())
				{
					c -= 32;
				}
				str.push_back(c);
			}
			else
			{
				str.push_back('#');
			}
		}
	}
	return str;
}

int Board::validateMove( Point oldPoint,  Point newPoint, const bool isBlack) const
{
	Piece* piece = getPieceIn(oldPoint);
	//cout << oldPoint._x << " " << oldPoint._y << endl;
	if (oldPoint == newPoint)
	{
		return NOT_VALID_DEST_EQUAL_ORIGIN;
	}
	if (piece==nullptr || piece->isBlack() != isBlack)
	{
		return NOT_VALID_NO_PIECE_IN_ORIGIN;
	}
	if (getPieceIn(newPoint) && getPieceIn(newPoint)->isBlack() == isBlack)
	{
		return NOT_VALID_PIECE_IN_DEST;
	}
	if (willThreaten(oldPoint, newPoint, isBlack))
	{
		return NOT_VALID_MOVE_WILL_CAUSE_CHESS;
	}
	if (getPieceIn(newPoint) && piece->canEat(newPoint))
	{
		//if it can eat i let the checking run normally
	}
	else if (getPieceIn(newPoint) || piece->validateMove(newPoint) != VALID_MOVE) //if the dest is not empty, and can't be eaten, it's not valid
	{
		return NOT_VALID_PIECE_ILLIGAL;
	}
	if (pieceBetween(oldPoint, newPoint))
	{
		return NOT_VALID_PIECE_IN_DEST;
	}
	if (willThreaten(oldPoint, newPoint, !isBlack))
	{
		return VALID_CHESS;
	}
	return VALID_MOVE;
}

void Board::move(const Point oldSpot, Point newSpot) 
{
	getPieceIn(oldSpot)->getPoint() = newSpot;
	delete getPieceIn(newSpot);
	_pieces[newSpot._y][newSpot._x] = getPieceIn(oldSpot);
	_pieces[oldSpot._y][oldSpot._x] = nullptr;

}

Piece * Board::getPieceIn(const Point p) const
{
	return _pieces[p._y][p._x];
}


//checks if the king is currently threatend
bool Board::isKingThreatned(const bool isBlack) const
{
	Piece* k = getKing(isBlack);
	bool isPieceBetween = false;
	for (int i = 0; i <= BOARD_SIDE; i++)
	{
		for (int j = 0; j <= BOARD_SIDE; j++)
		{
			isPieceBetween = _pieces[i][j] ? pieceBetween(Point(i, j), k->getPoint()) : false; //if there is a piece there check if it's in the way, otherwise it's not
			if (_pieces[i][j] && !isPieceBetween && _pieces[i][j]->isBlack() != isBlack && _pieces[i][j]->canEat(k->getPoint()))
			{
				return true;
			}
		}
	}
	return false;
}

Piece * Board::getKing(const bool isBlack)const
{
	for (int i = 0; i <= BOARD_SIDE; i++)
	{
		for (int j = 0; j <= BOARD_SIDE; j++)
		{
			if (_pieces[i][j] && _pieces[i][j]->getType() == 'k' && _pieces[i][j]->isBlack() == isBlack)
			{
				return _pieces[i][j];
			}
		}
	}
	return nullptr;
}

void Board::printBoard() const
{
	Piece* p;
	char c;
	for (int i = 0; i <= BOARD_SIDE; i++)
	{
		for (int j = 0; j <= BOARD_SIDE; j++)
		{
			p = _pieces[i][j];
			if (p)
			{
				c = p->getType();
				if (!p->isBlack())
				{
					c -= 32;
				}
				printf("%5c(%d,%d)%5", c ,p->getPoint()._x , p->getPoint()._y);
			}
			else
			{
				printf("%10c", '#');
			}
		}
		cout << endl << endl;
	}
}

void Board::printBetween(const Point old, const Point newp) const
{
	vector<Point*> myVec = _pieces[old._y][old._x] ? _pieces[old._y][old._x]->getBetween(newp) : *new vector<Point*>;
	for (int i = 0; i < myVec.size(); i++)
	{
		cout << "(" << myVec[i]->_x << myVec[i]->_y << ")" << endl;
	}
}

bool Board::pieceBetween(const Point old, const Point newp)const
{
	Piece* piece = getPieceIn(old);
	vector<Point*> myVec = piece->getBetween(newp);
	for (int i = 0; i < myVec.size(); i++)
	{
		if (getPieceIn(*myVec[i]) != nullptr)
		{
			delete myVec[i];
			return true;
		}
		delete myVec[i];
	}
	return false;
}

bool Board::willThreaten(const Point oldPoint, const Point newPoint, const bool isBlack) const
{
	Board myBoard(getBoardStr());
	myBoard.move(oldPoint, newPoint); //because i'm getting im making a new one i don't care about validity
	if (myBoard.isKingThreatned(isBlack))
	{
		return true;
	}
	return false;
}
