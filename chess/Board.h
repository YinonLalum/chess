#pragma once
#include "Piece.h"
#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Queen.h"
#include "King.h"
#include "Rook.h"
#include "Pawn.h"

#define BOARD_SIDE 7

class Board
{
private:
	Piece* _pieces[BOARD_SIDE+1][BOARD_SIDE+1];

public:
	Board(std::string str);
	Board(Board& b);
	~Board();
	std::string getBoardStr() const;
	int validateMove( Point oldPoint,  Point newPoint, const bool isBlack) const;
	void move(const Point oldSpot, Point newSpot) ;
	Piece* getPieceIn(const Point p) const;
	bool willThreaten(const Point oldPoint, const Point newPoint, const bool isBlack) const;
	bool isKingThreatned(const bool isBlack) const;
	Piece* getKing(const bool isBlack) const;
	void printBoard() const;
	void printBetween(const Point old, const Point newp) const;
	bool pieceBetween(const Point old, const Point newp) const;
	friend Point::Point(std::string s);
};