#include "King.h"

King::King(const bool isBlack, const Point p) : Piece(isBlack,p)
{
	_type = 'k';
}

int King::validateMove(const Point newPoint)
{
	Point difference(newPoint);
	difference -= _loc;
	if ((difference._x >= -1 && difference._x <= 1) &&
		(difference._y >= -1 && difference._y <= 1))
	{
		return VALID_MOVE;
	}
	return NOT_VALID_PIECE_ILLIGAL;
}


