#pragma once
#include "Piece.h"
#include "Board.h"

class Board; //breaking the circular dependency

class King : public Piece
{
public:
	King(const bool isBlack, const Point p);
	int validateMove(const Point newPoint) ;
	//king can only move one slot so there is no between
};