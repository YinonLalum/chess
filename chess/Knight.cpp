#include "Knight.h"


Knight::Knight(const bool isBlack, const Point p) : Piece(isBlack, p)
{
	_type = 'n';
}

int Knight::validateMove(const Point newPoint) 
{
	if (sqrt(pow(newPoint._x - _loc._x, 2) + pow(newPoint._y - _loc._y, 2)) == sqrt(5))
	{
		return VALID_MOVE;
	}
	return NOT_VALID_PIECE_ILLIGAL;
}
