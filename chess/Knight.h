#pragma once
#include "Piece.h"
#include <math.h>

class Knight : public Piece
{
public:
	Knight(const bool isBlack, const Point p);
	int validateMove(const Point newPoint) ;
	//while he moves between some Points, i don't care about them because he can skip above Pieces

};



