#include "Pawn.h"

Pawn::Pawn(const bool isBlack, const Point p) : Piece(isBlack, p)
{
	_type = 'p';
	_isFirstTurn = true;
}

int Pawn::validateMove(const Point newPoint)
{
	int direction = _isBlack ? 1 : -1;
	int dirPlus1 = _isBlack ? 2 : -2; //if it's white i want to negate the result, because the y is opposite
	if (_loc._x == newPoint._x)
	{
		if (_isFirstTurn)
		{
			if (newPoint._y - _loc._y == direction || newPoint._y - _loc._y == dirPlus1)
			{
				_isFirstTurn = false;
				return VALID_MOVE;
			}
		}
		if (newPoint._y - _loc._y == direction)
		{
			return VALID_MOVE;
		}
	}
	return NOT_VALID_PIECE_ILLIGAL;
}

bool Pawn::canEat(const Point newPoint)
{
	int direction = _isBlack ? 1 : -1;

	if ((_loc._x + direction == newPoint._x || _loc._x - direction == newPoint._x) && newPoint._y - _loc._y == direction)
	{
		return true;
	}
	return false;
}

std::vector<Point*> Pawn::getBetween(Point newPoint) 
{
	std::vector<Point*> myVec;
	Point direction = newPoint - _loc;
	int y = _loc._y;
	int x = _loc._x;
	while (y != newPoint._y)
	{
		direction._y < 0 ? y-- : y++;
		myVec.push_back(new Point(y, x));
	}
  	if (myVec.size() > 0)
	{
		delete myVec.back();
		myVec.pop_back(); //because the last point would be newPoint
	}
	return myVec;
}




