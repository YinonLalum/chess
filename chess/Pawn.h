#pragma once
#include "Piece.h"


class Pawn : public Piece
{
public:
	Pawn(const bool isBlack, const Point p);
	int validateMove(const Point newPoint) ;
	bool canEat(const Point newPoint);
	std::vector<Point*> getBetween(Point newPoint) ;

private:
	bool _isFirstTurn;
};
