#pragma once
#include "Point.h"
#include <iostream>
#include "vector"

#define VALID_MOVE 0
#define VALID_CHESS 1
#define NOT_VALID_NO_PIECE_IN_ORIGIN 2
#define NOT_VALID_PIECE_IN_DEST 3
#define NOT_VALID_MOVE_WILL_CAUSE_CHESS 4
#define NOT_VALID_OUT_OF_INDEX 5 
#define NOT_VALID_PIECE_ILLIGAL 6
#define NOT_VALID_DEST_EQUAL_ORIGIN 7

class Piece
{
protected:
	bool  _isBlack; //0 is white, anything else is black
	char _type;
	Point _loc;

public:
	Piece(const bool isBlack, const Point p) : _loc(p)
	{
		_isBlack = isBlack;
	};
	
	virtual ~Piece()
	{

	};

	virtual int validateMove(const Point newSpot) = 0;
	//checks if piece can move according to the pieces rules, assuming that the board is empty
	//can return 2 values:  VALID_MOVE when the move is valid
	//						NOT_VALID_PIECE_ILLLIGAL when the move is not in accordance to the piece's rules
	
	virtual std::vector<Point*> getBetween(Point newPoint) 
	{
		std::vector<Point*> myVec;
		return myVec;
	}

	virtual bool canEat(const Point newPoint)
	{
		if (validateMove(newPoint) == VALID_MOVE) //because for every piece except the Pawn operates this way
		{
			return true;
		}
		return false;
	};

	bool isBlack() const
	{
		return _isBlack;
	}


	Point& getPoint()
	{
		return _loc;
	}

	char getType() const
	{
		return _type;
	}

};