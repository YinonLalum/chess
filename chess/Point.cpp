#include "Point.h"

Point::Point(std::string s)
{
	_x = (int)s[0] - 97;                    //97 is the letter char offset in ascii
	_y = -1*((int)s[1] - 48 - BOARD_SIDE);  //48 is the number char offset in ascii, *-1 because my 0,0 is in the top left
								            //and the number passed is considering 0,0 is in bottom left
	if (_y < 0 || _y >= BOARD_SIDE || _x < 0 || _x >= BOARD_SIDE)
	{
		throw std::exception("out of board borders");
	}
}

Point::Point(int y, int x)
{
	_x = x;
	_y = y;
}

Point::~Point()
{
}

Point Point::operator+(Point & other)
{
	Point p(0,0);
	p += other;
	p += *this;
	return p;
}

Point & Point::operator+=(Point & other)
{
	_x = other._x + _x;
	_y = other._y + _y;
	return *this;
}

Point Point::operator-(Point & other)
{
	Point p(_y, _x);
	p -= other;
	return p;
}

Point & Point::operator-=(Point & other)
{
	_x = _x - other._x;
	_y = _y - other._y;
	return *this;
}

Point& Point::operator=(Point& other)
{
	_x = other._x;
	_y = other._y;
	return *this;
}

bool Point::operator==(Point & other)
{
	if (_x == other._x && _y == other._y)
	{
		return true;
	}
	return false;
}

