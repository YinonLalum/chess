#pragma once
#include <iostream>

#define BOARD_SIDE 8

class Point
{
public:
	int _x;
	int _y;

	Point(std::string s);
	Point(int y, int x);
	~Point();
	Point operator+(Point& other);
	Point& operator+=(Point& other);
	Point operator-(Point& other);
	Point& operator-=(Point& other);
	Point& operator=(Point& other);
	bool operator==(Point& other);
};