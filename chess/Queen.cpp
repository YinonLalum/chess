#include "Queen.h"

Queen::Queen(const bool isBlack, const Point p) : Rook(isBlack, p) , Bishop(isBlack,p), Piece(isBlack, p)
{
	Piece::_type = 'q';
}

int Queen::validateMove(const Point newPoint)
{
	int res1 = Bishop::validateMove(newPoint);
	int res2 = Rook::validateMove(newPoint);
	return res1 == VALID_MOVE || res2 == VALID_MOVE ? VALID_MOVE : NOT_VALID_PIECE_ILLIGAL;
}

std::vector<Point*> Queen::getBetween(Point newPoint)
{
	if (newPoint._x == Piece::_loc._x || newPoint._y == Piece::_loc._y) //checking if rook
	{
		return Rook::getBetween(newPoint);
	}
	return Bishop::getBetween(newPoint);
}

