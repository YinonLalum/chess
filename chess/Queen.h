#pragma once
#include "Piece.h"
#include "Bishop.h"
#include "Rook.h"

class Queen : public Rook, public Bishop
{

public:
	Queen(const bool isBlack, const Point p);
	std::vector<Point*> getBetween(Point newPoint);
	int validateMove(const Point newPoint);
};
