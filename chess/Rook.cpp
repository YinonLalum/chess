#include "Rook.h"



Rook::Rook(const bool isBlack, const  Point p) : Piece(isBlack, p)
{
	_type = 'r';
}

int Rook::validateMove(const Point newSpot)
{
	if (_loc._x == newSpot._x || _loc._y == newSpot._y) //because i already checked if they are completely euqal, and they are not
															  //if one coordinate is equal, the second must be different
	{
		return VALID_MOVE;
	}
	return NOT_VALID_PIECE_ILLIGAL;
}

std::vector<Point*> Rook::getBetween(Point newPoint)
{
	std::vector<Point*> myVec;
	Point direction = newPoint - _loc;
	int y = _loc._y;
	int x = _loc._x;
	if (newPoint._x == x)
	{
		while (y != newPoint._y)
		{
			direction._y < 0 ? y-- : y++;
			myVec.push_back(new Point(y, x));
		}
		delete myVec.back();
		myVec.pop_back(); //because the last point would be newPoint
	}
	else
	{
		while (x != newPoint._x)
		{
			direction._x < 0 ? x-- : x++;
			myVec.push_back(new Point(y, x));
		}
		if (myVec.size() > 0)
		{
			delete myVec.back();
			myVec.pop_back(); //because the last point would be newPoint
		}
	}
	return myVec;
}
