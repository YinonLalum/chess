#pragma once
#include "Piece.h"

class Rook : public virtual Piece
{
public:
	Rook(const bool isBlack, const Point p);
	int validateMove(const Point newSpot);
	std::vector<Point*> getBetween(Point newPoint);
};