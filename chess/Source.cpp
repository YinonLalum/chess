#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Board.h"
#pragma warning(disable:4996) //this allowes me to use strcpy rather then strcpy_s

using namespace std;

void main()
{
	srand(time_t(NULL));
	Pipe p;
	bool isConnect = p.connect();
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;
		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"); // just example...
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	Board board(msgToGraphics);
	Point to(0,0);
	Point from(0,0);
	int result = 0;
	string toSend;
	bool isBlackTurn = true;

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		
		try
		{
			from = *&Point(msgFromGraphics.substr(0, 2));
			to   = *&Point(msgFromGraphics.substr(2, 2));
		}
		catch (exception e)
		{
			cout << e.what() << endl;
			result = NOT_VALID_OUT_OF_INDEX;
			goto convertToCArr; //i want to skip the moving procedure if the error was throwen
			//goto is cpp's equivilant for assembly jmp
		}

		board.printBoard();
		result = board.validateMove(from, to, isBlackTurn);
		
		if (result == VALID_CHESS || result == VALID_MOVE)
		{
			board.printBetween(from, to);
			board.move(from, to);
			isBlackTurn = !isBlackTurn;	//toggeling the turn	
		}

		convertToCArr:

		//converting to a C array
		toSend = to_string(result);
		char *cstr = new char[toSend.length() + 1];
		strcpy(cstr, toSend.c_str());
		strcpy_s(msgToGraphics, cstr); // msgToGraphics should contain the result of the operation
		delete[] cstr;
		


		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}