0 � ���� ����. ������ �����. 
1 � ���� ����. ������ ����� ���� ���� ��� �� �����.
2 � ���� �� ����. ������ ����� ��� ��� �� ����� ������
3 � ���� �� ����. ������ ���� ���� ��� �� ����� ������.
4 � ���� �� ����. ������ ������ ���� �� �� ����� ������
5 � ���� �� ����. �������� �� ������� ���� ������. ��
6 � ���� �� ����. ����� �� ����� �� ����. ��
7 � ���� �� ����. ����� ����� ������ ���� ���� ��
8 � ���� ����. ��-�� (�����)

k � ���
q � ����
r � ����
n � ���
b � ��
p � ����
# - ����� ����
��� �-65 ����� ���� ���� �����. 0 - ���� ���, ��� - ���� ����.
��� �-66 ��� NULL

class Point
int _x
int _y
operator overload for +, += ,- , =, -=

class: Board
Piece*[7][7] _pieces;

int validateMove(Point oldSpot, Point newSpot);
void move(Point oldSpot, Point newSpot);
bool threatenKing(Point newSpot);
void eat(Point newPiece);

abstract class Piece
char  _isBlack (0 is white, anything else is black)

virtual int validateMove(Point oldSpot, Point newSpot) = 0; 
//checks if piece can move according to the pieces rules, assuming that the board is empty

virtual bool canEat(Point oldSpot, Point newPoint);
class King: Piece 
class Queen: Piece
class Rook: Piece
class Bishop: Piece
class Knight: Piece
class Pawn: Piece -
bool canEat(Point oldSpot, Point newPoint);


